# Blog do Roberto

Este é o repositório do meu blog pessoal, que pode ser acessado em:

> https://rcisne.codeberg.page/blog

Para ver o código fonte, você deve selecionar a branch *pages* (em vez da *main*).
O link direto é <https://codeberg.org/rcisne/blog/src/branch/pages>.